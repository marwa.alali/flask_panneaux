import io
import PIL
from PIL import Image
import os
from os import path
from flask import Flask, request , render_template
import numpy as np
import tensorflow as tf
import io
from werkzeug.utils import secure_filename
from keras.models import load_model
from flask import send_from_directory



def transform_image(img):
    test_predict = Image.open(io.BytesIO(img)).resize((28, 28))
    test_predict = np.array(test_predict).astype("float32")/255.0
    test_predict = test_predict.reshape(1, 28, 28, 3)
    return test_predict




def get_predictions(img):
    image_bytes = transform_image(img)
    model_loaded = tf.keras.models.load_model("Best_model_20.h5")
    prediction = model_loaded.predict(image_bytes)
    proba_array = prediction.argsort()
    result = proba_array[0]
    return result, prediction

